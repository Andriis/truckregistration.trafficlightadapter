﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using NLog;

namespace TruckRegistration.TrafficLightController
{
    public partial class TrafficLightController : ServiceBase
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private Timer _timer = new Timer();
        private Timer _timer2 = new Timer();
        private List<vwTrafficLight> _controllers = new List<vwTrafficLight>();
        private List<Equipment> _equipments = new List<Equipment>();
        private List<TrafficLight> _trafficLights = new List<TrafficLight>();

        public TrafficLightController()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
             _logger.Trace("Инициализация сервера");
                     
             _timer.Interval = 1000;
             _timer.Elapsed += _timer_Elapsed;
             _timer.Start();

             _timer2.Interval = 10000;
             _timer2.Elapsed +=_timer2_Elapsed; ;
             _timer2.Start();

             using (var dl = new OMS())
             {
                 _controllers = dl.vwTrafficLights.ToList();
                 _equipments = dl.Equipments.Where(e => e.EquipmentTypeId == 4).ToList();
             }

             Init();

            _logger.Trace("Сервис запущен.");
        }

        private void _timer2_Elapsed(object sender, ElapsedEventArgs e)
        {
            _timer2.Stop();
            SwitchRedLightByTimeout();
            _timer2.Start();
        }
     
        void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Stop();
            CheckNewTask();
            _timer.Start();
        }

        private void Init()
        {
            using (var dl = new OMS())
            {

                var trafficLightControllers = dl.vwTrafficLightControllers;

                foreach (var trafficLightController in trafficLightControllers)
                {
                    try
                    {
                        
                        AddTrafficLightController(trafficLightController);

                    }
                    catch (Exception ex)
                    {
                        _logger.Error("Ошибка во время инициализации светофоров");
                        _logger.Error(ex);
                    }
                }
            }
        }

        private void CheckNewTask()
        {
            using (var dl = new OMS())
            {

                var actualLighteTasks = dl.TrafficLightTasks.Where(t => t.IsNewRecord == true);

                foreach (var task in actualLighteTasks)
                {
                    if (!string.IsNullOrEmpty(task.EquipmentCode))
                    {

                        var equipment = _equipments
                                .Single(eq => eq.Code == task.EquipmentCode);

                        if (equipment != null)
                        {
                            var controller = _controllers.
                                Single(c => c.ID == equipment.ID);
                            if (controller != null)
                            {
                                try
                                {
                                    var semaphore =
                                     new TruckRegistration.Devices
                                         .ServiceLibrary.TrafficLight(
                                           controller.ID,
                                           controller.ControllerIP,
                                           controller.Port,
                                           controller.HeartBitPort);

                                    _logger.Trace("ID={0} ControllerIP={1} Port={2} HeartBitPort={3}"
                                        , controller.ID,
                                           controller.ControllerIP,
                                           controller.Port,
                                           controller.HeartBitPort);

                                    if (semaphore.IsConnected)
                                    {
                                        semaphore.SetGreeOn(controller.Value);
                                        using (var dataLayer = new OMS())
                                        {
                                            var newEquipmentLog = new EquipmentStatusLog
                                            {
                                                DateTime = DateTime.Now,
                                                EquipmentStatusTypeId = 2, // GREENLIGHT
                                                EquipmentId = controller.ID
                                            };
                                            dataLayer.EquipmentStatusLogs.AddObject(newEquipmentLog);
                                            dataLayer.SaveChanges();

                                            var updateTask = dataLayer.TrafficLightTasks.Single(t => t.Id == task.Id);
                                            updateTask.IsNewRecord = false;
                                            updateTask.ReadDate = DateTime.Now;
                                            dataLayer.SaveChanges();
                                        }
                                        _logger.Trace("Переключение прошло успешно" + task.EquipmentCode);


                                    }
                                    else
                                    {
                                        var newEquipmentLog = new EquipmentStatusLog
                                        {
                                            DateTime = DateTime.Now,
                                            EquipmentStatusTypeId = 1, // NOTWORK
                                            EquipmentId = controller.ID
                                        };
                                        dl.EquipmentStatusLogs.AddObject(newEquipmentLog);
                                        var newEventLog = new TruckRegistration.DataModel.EventLog
                                        {
                                            EventDate = DateTime.Now,
                                            SourceName = string.Format("Переключение светофора EquipmentCode:{0} ID:{1} value:{2}", task.EquipmentCode, controller.ID, controller.Value),
                                            Description = "Отсутствие связи с контоллером",
                                        };
                                        dl.EventLogs.AddObject(newEventLog);
                                        dl.SaveChanges();
                                        _logger.Trace(string.Format("Переключение светофора EquipmentCode:{0} ID:{1} value:{2}", task.EquipmentCode, controller.ID, controller.Value));
                                    }
                                }
                                catch (Exception ex)
                                {
                                    _logger.Error(ex);
                                    var newEquipmentLog = new EquipmentStatusLog
                                    {
                                        DateTime = DateTime.Now,
                                        EquipmentStatusTypeId = 15, // ERROR
                                        EquipmentId = controller.ID
                                    };
                                    dl.EquipmentStatusLogs.AddObject(newEquipmentLog);
                                    var newEventLog = new TruckRegistration.DataModel.EventLog
                                    {
                                        EventDate = DateTime.Now,
                                        SourceName = "Переключение светофора c ошибкой",
                                        Description = ex.Message,
                                    };
                                    dl.EventLogs.AddObject(newEventLog);
                                }
                            }

                        }
                    }
                }
            }
        }

        private void AddTrafficLightController(vwTrafficLightController trafficLightController)
        {
            if (!_trafficLights.Any(t => t.EquipmentId == trafficLightController.ID))
            {
                var newTrafficLight = new TrafficLight(trafficLightController.ID,
                                   trafficLightController.ControllerIP,
                                   trafficLightController.Port,
                                   trafficLightController.HeartBitPort);

                newTrafficLight.WriteStatusBit();

                OnComplete(trafficLightController.ID, newTrafficLight.IsConnected);

                _trafficLights.Add(newTrafficLight);
            }
        }



        /// <summary>
        /// Switches red light after GreenLight timeout period of illumination.
        /// </summary>
        public void SwitchRedLightByTimeout()
        {
            using (var dl = new TruckRegistration.DataModel.OMS())
            {
                var trafficLightControllers = dl.vwTrafficLightControllers;
                var timeOutSeconds = Properties.Settings.Default.GreenLightTimeOutInSeconds;
                var timeOut = DateTime.Now.AddSeconds(-timeOutSeconds);

                var isActualGreenLights = dl.TrafficLightTasks.Where(t => t.IsNewRecord == false)
                                .Where(t => t.ReadDate.Value > timeOut);

                foreach (var trafficLightController in trafficLightControllers)
                {
                    try
                    {
                        var trafficLight = _trafficLights
                            .FirstOrDefault(t => t.EquipmentId == 
                                trafficLightController.ID);
                        if (trafficLight != null)
                        {
                            if (!trafficLight.IsConnected)
                            {
                                trafficLight.ReConnect();
                            }
                            var redAddreses = dl.vwTrafficLights
                                .Where(tl => tl.ControllerIP == 
                                    trafficLightController.ControllerIP 
                                    );

                            foreach (var addres in redAddreses)
                            {
                                bool isGreen = false;
                                var eq =  _equipments.SingleOrDefault(e=>e.ID==addres.ID);
                                if (eq!=null)
	                            {
                                    isGreen=isActualGreenLights.Any(e => e.EquipmentCode == eq.Code);
	                            }

                                try
                                {
                                    trafficLight.WriteStatusBit();
                                }
                                catch (Exception ex)
                                {
                                    trafficLight.ReConnect();
                                    trafficLight.WriteStatusBit();
                                }

                                if (trafficLight.IsConnected && !isGreen)
                                {
                                    //trafficLight.WriteStatusBit();
                                    try
                                    {
                                        trafficLight.SetRedOn(addres.Value);
                                    }
                                    catch (Exception ex)
                                    {
                                        try
                                        {
                                            trafficLight.ReConnect();
                                            trafficLight.SetRedOn(addres.Value);
                                        }
                                        catch (Exception exx)
                                        {
                                           
                                        }
                                        
                                    }
                                }
                            }
                            OnComplete(trafficLight.EquipmentId, trafficLight.IsConnected);
                        }
                        else
                        {
                            AddTrafficLightController(trafficLightController);
                        }

                    }
                    catch (Exception ex)
                    {
                        _logger.LogException(LogLevel.Error, string.Format("Ошибка во время проверки светофора EquipmentId:{0}", trafficLightController.ID), ex);
                    }
                }
            }
        }

        /// <summary>
        /// Processing of the results, after the completion of the all threads.
        /// </summary>
        /// <param name="EquipmentId">The equipment unique identifier.</param>
        /// <param name="isSave">if set to <c>true</c> [is save].</param>
        private void OnComplete(long EquipmentId, bool enableStatus)
        {
            try
            {
                using (var dl = new TruckRegistration.DataModel.OMS())
                {
                    EquipmentStatusType statusEquipmentNotWork =
                       dl.EquipmentStatusTypes.First(est => est.Alias == "NOTWORK");

                    EquipmentStatusType statusEquipmentReady =
                        dl.EquipmentStatusTypes.First(est => est.Alias == "READY");

                    dl.EquipmentCurrentStatus.Single(e => e.EquipmentId == EquipmentId)
                        .EquipmentStatusTypeId = enableStatus ?
                        statusEquipmentReady.ID : statusEquipmentNotWork.ID;

                    dl.SaveChanges();
                }
                _logger.Info(string.Format("Перевірено сполучення з контроллером ID:{0} Status:{1}", EquipmentId, enableStatus));

            }
            catch (Exception ex)
            {
                _logger.Error(ex);

            }
        }
      
        protected override void OnStop()
        {
            if (_timer!=null)
            {
                 _timer.Stop();
                 _timer.Dispose();
                 _timer = null;
            }
           
            _logger.Trace("Сервис остановлен.");
        }
    }
}
